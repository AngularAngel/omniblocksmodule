#version 140

in vec3 in_pos; //Supplied by the vertex buffer.

uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;

void main() {
    vec4 pos = vec4(in_pos, 1.0);
    gl_Position = u_projection_matrix*u_view_matrix*u_model_matrix*pos;
}