#version 140

uniform sampler2D u_position_tex;
uniform sampler2D u_normal_tex;
uniform sampler2D u_color_tex;
uniform sampler2D u_depth_tex;

uniform samplerCubeShadow u_shadow_tex;

uniform vec2 u_screen_size;

uniform vec3 u_camera_pos;
uniform vec3 u_light_position;
uniform vec3 u_light_color;

uniform float u_light_linear;
uniform float u_light_quadratic;
uniform float u_light_radius;
uniform float u_z_far;
uniform float u_shadow_z_near;
uniform float u_shadow_res;
uniform float u_texel_length;

uniform bool u_has_shadows;

out vec3 out_color;

const float SHADOW_BIAS = exp2(-23);
const float SHADOW_SLOPE_BIAS = exp2(-22);

vec2 calcTexCoord() {
   return gl_FragCoord.xy / u_screen_size;
}

float z_to_depth(float n, float f, float z) {
    float z_range = f - n;
    float ndc_z = (f + n) / z_range - (2.0 * f * n) / (z * z_range);
    return ndc_z * 0.5 + 0.5;
}

void main() {
    vec2 v_tex_coord = calcTexCoord();

    // retrieve data from G-buffer
    vec3 v_pos = texture(u_position_tex, v_tex_coord).rgb;
    vec3 texel_pos = floor(v_pos) + floor(fract(v_pos) * (u_texel_length * 2)) / (u_texel_length * 2);
    vec3 normal = texture(u_normal_tex, v_tex_coord).rgb;
    vec3 color = texture(u_color_tex, v_tex_coord).rgb;
    float depth = texture(u_depth_tex, v_tex_coord).r * u_z_far;
    
    vec3 light_vector = u_light_position - v_pos;
    
    vec3 light_dir = normalize(light_vector);
    
    float cosang = dot(normal, light_dir);
    
    if (cosang <= 0.0)
        discard;
    
    float shadow = 1;
    if (u_has_shadows) {
        float shadow_view_z = max(max(abs(light_vector.x), abs(light_vector.y)), abs(light_vector.z));

        float slope = sqrt(1.0 - cosang * cosang) / cosang;
        float shadow_texel_size = sqrt(3.0) * shadow_view_z / u_shadow_res;
        float shadow_bias = (slope + 1.0) * shadow_texel_size;

        float shadow_depth = z_to_depth(u_shadow_z_near, u_light_radius, shadow_view_z - shadow_bias);
        shadow = texture(u_shadow_tex, vec4(-light_dir, shadow_depth));
    }
    
    if (shadow <= 0.0) discard;

    vec3 diffuse = max(dot(normal, light_dir), 0.0) * color * u_light_color;

    float distance = length(u_light_position - texel_pos);

    float attenuation = 1.0 / (1.0 + u_light_linear * distance + u_light_quadratic * distance * distance);
    out_color = diffuse * shadow * attenuation;
}