#version 140

uniform sampler2D u_position_tex;
uniform sampler2D u_normal_tex;
uniform sampler2D u_color_tex;
uniform sampler2D u_depth_tex;
uniform sampler2D u_light_tex;

uniform float u_z_far;
uniform int u_overflow_factor;

in vec2 v_tex_coord;

out vec3 out_color;

void main() {
    // retrieve data from G-buffer
    vec3 texel_pos = texture(u_position_tex, v_tex_coord).rgb;
    vec3 normal = texture(u_normal_tex, v_tex_coord).rgb;
    vec3 color = texture(u_color_tex, v_tex_coord).rgb;
    float depth = texture(u_depth_tex, v_tex_coord).r * u_z_far;
    vec3 light = texture(u_light_tex, v_tex_coord).rgb * u_overflow_factor;
    
    if (length(normal) == 0)
        discard;
    
    out_color = color * light;
}