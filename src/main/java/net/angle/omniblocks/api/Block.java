
package net.angle.omniblocks.api;

import net.angle.omniregistry.api.Renderable;

/**
 *
 * @author angle
 * @license https://gitlab.com/AngularAngel/omnicraft/-/blob/master/LICENSE
 */
public interface Block extends Renderable {
    public default boolean isVisibleThrough(Block adjacentBlock) {
        return adjacentBlock == null || (adjacentBlock.isTransparent() && adjacentBlock.getId() != getId());
    }
    
    @Override
    public default boolean isTransparent() {
        return false;
    }
    
    @Override
    public default boolean isDrawable() {
        return true;
    }
    
    public default boolean isSolid() {
        return false;
    }
}