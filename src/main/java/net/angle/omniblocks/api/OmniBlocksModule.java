package net.angle.omniblocks.api;

import net.angle.omnimodule.api.OmniModule;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
public interface OmniBlocksModule extends OmniModule {
    public void populateRegistries(RegistryCollection registries);
    public void prepSerializerRegistry(ObjectSerializerRegistry serializerRegistry);
}
