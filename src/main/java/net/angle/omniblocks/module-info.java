
module omni.blocks {
    requires static lombok;
    requires java.logging;
    requires omni.registry;
    requires devil.util;
    requires omni.module;
    requires omni.world;
    requires omni.serialization;
    requires org.lwjgl.opengl;
    exports net.angle.omniblocks.api;
    exports net.angle.omniblocks.impl;
}