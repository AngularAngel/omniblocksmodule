package net.angle.omniblocks.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omniblocks.api.Block;
import net.angle.omniblocks.api.OmniBlocksModule;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniregistry.impl.BasicDatumRegistry;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.impl.chunks.BasicDatumVoxelComponent;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniserialization.impl.ReflectiveSerializer;

/**
 *
 * @author angle
 */
public class OmniBlocksModuleImpl extends BasicOmniModule implements OmniBlocksModule {

    public OmniBlocksModuleImpl() {
        super(OmniBlocksModule.class);
    }

    @Override
    public void populateRegistries(RegistryCollection registries) {
        Registry<Block> blocksRegistry = (Registry<Block>) registries.addEntry(new BasicDatumRegistry<>("Blocks", registries));
        
        Registry<VoxelComponent> chunkComponents = (Registry<VoxelComponent>) registries.getEntryByName("Chunk Components");
        
        chunkComponents.addEntry(new BasicDatumVoxelComponent<>("Blocks", chunkComponents, Block.class, blocksRegistry));
    }
    
    @Override
    public void prepSerializerRegistry(ObjectSerializerRegistry serializerRegistry) {
        try {
            serializerRegistry.registerObject(BasicBlock.class, new ReflectiveSerializer<>(BasicBlock.class.getConstructor(String.class, int.class, int.class, int.class, boolean.class, boolean.class, boolean.class), (t) -> {
                return new Object[]{t.getName(), t.getRegistryID(), t.getId(), t.getTextureDefinitionID(), t.isTransparent(), t.isDrawable(), t.isSolid()};
            }));
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(OmniBlocksModuleImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}