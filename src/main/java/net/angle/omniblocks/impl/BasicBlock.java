/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniblocks.impl;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import net.angle.omniblocks.api.Block;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.impl.AbstractRenderable;

/**
 *
 * @author angle
 * @license https://gitlab.com/AngularAngel/omnicraft/-/blob/master/LICENSE
 */
@EqualsAndHashCode(callSuper = true)
public class BasicBlock extends AbstractRenderable implements Block {
    private final @Getter boolean solid;
    
    public BasicBlock(String name, Registry registry, int textureDefinitionID, boolean transparent, boolean drawable, boolean solid) {
        super(name, registry, textureDefinitionID, transparent, drawable);
        this.solid = solid;
    }
    
    public BasicBlock(String name, Registry registry, int textureDefinitionID) {
        super(name, registry, textureDefinitionID, false, true);
        this.solid = false;
    }
    
    public BasicBlock(String name, int registryID, int id, int textureDefinitionID, boolean transparent, boolean drawable, boolean solid) {
        super(name, registryID, id, textureDefinitionID, transparent, drawable);
        this.solid = solid;
    }
    
    public BasicBlock(String name, int registryID, int id, int textureDefinitionID) {
        super(name, registryID, id, textureDefinitionID, false, true);
        this.solid = false;
    }
    
    @Override
    public String toString() {
        return "BasicBlock(" + getName() + ", TextureID: " + getTextureDefinitionID() + ", Solid: " + isSolid() + ", Drawable: " + isDrawable() + ", Transparent: " + isTransparent() + ")";
    }
}