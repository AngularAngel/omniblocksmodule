package net.angle.omniblocks.impl;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.angle.omniregistry.impl.AbstractRenderableBuilder;

/**
 *
 * @author angle
 */
@ToString
public class BasicBlockBuilder extends AbstractRenderableBuilder<BasicBlock> {
    private @Getter @Setter boolean solid;
    public BasicBlockBuilder() {
        super(BasicBlock.class);
    }

    @Override
    public BasicBlock build() {
        checkBuildability();
        return build(new BasicBlock(getName(), getRegistry(), getTextureDefinitionID(), isTransparent(), isDrawable(), isSolid()));
    }
}